#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2021-2022
# Gener 2022
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select, os
from subprocess import Popen, PIPE

HOST = ''                 
PORT = 50002
FI = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
pid=os.fork()
if pid !=0:
    print("Engegant server PS:",pid)
    sys.exit(0)
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            command = conn.recv(1024)
            print ("telnet> %s" % (command))
            if not command: break
            pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
            for line in pipeData.stdout:
                print(line)
                conn.send(line)
            for line in pipeData.stderr:
                print("Error: %s" % line)
                conn.send(line)
            conn.send(FI)
s.close()
sys.exit(0)
