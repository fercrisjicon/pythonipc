# /usr/bin/python3
#-*- coding: utf-8-*-
#
# telnet-server.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse, socket, os, signal, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Telnet Server""")
parser.add_argument("-p","--port", type=int, default="50001")
parser.add_argument("-d","--debug",action='store_true',default=False)
args = parser.parse_args()
HOST = ''
PORT = args.port
FI = bytes(chr(4), 'utf-8')

pid=os.fork()
if pid !=0:
    print("Engegant server PS:",pid)
    sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print ("Connected by: %s" %(addr[0]))
    while True:
        command = conn.recv(1024)
        if args.debug:
            print ("telnet> %s" % (command))
        if not command: break
        pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
        for line in pipeData.stdout:
            if args.debug:
                print(line)
            conn.send(line)
        for line in pipeData.stderr:
            if args.debug:
                print("Error: %s" % line)
            conn.send(line)
        conn.send(FI)
    conn.close()
#s.close()
sys.exit(0)
