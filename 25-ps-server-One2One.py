# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse, socket, os, signal, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """PS Server""")
parser.add_argument("-p","--port", type=int, default="50001")
args = parser.parse_args()
HOST = ''
PORT = args.port

pid=os.fork()
if pid !=0:
    print("Engegant server PS:",pid)
    sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    nomFile = "/tmp/%s_%s_%s.log" % (addr[0],addr[1],time.strftime("%Y%m%d-%H%M%S"))
    fileIn=open(nomFile,"w")
    while True:
        data = conn.recv(1024)
        if not data: break
        fileIn.write(str(data))
    conn.close()
    fileIn.close()
