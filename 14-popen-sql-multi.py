# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
# -------------------------------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta SQL interactiva""",\
        epilog="thats all folks")
parser.add_argument("-d","--database", help="base de dades a usar",\
     default="training")
parser.add_argument("-c", "--client", help="Codis clients que volem llistar",\
        type=str, action="append", dest="clieList", required="True")
args = parser.parse_args()
# -------------------------------------------------------
command = " psql -qtA -F',' -h 172.17.0.2 -U postgres "+ args.database
pipeData = Popen(command,shell=True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.clieList:
    seqSQL="select * from clientes where num_clie=%s;" % (num_clie)
    pipeData.stdin.write(seqSQL+"\n")
    print(pipeData.stdout.readline(),end="")

pipeData.stdin.write("\q")
exit(0)
