# /usr/bin/python
#-*- coding: utf-8-*-
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
# -------------------------------------------------------
parser = argparse.ArgumentParser(description=\
        """Consulta SQL interactiva""",\
        epilog="thats all folks")
parser.add_argument("sequenciaSQL", help="sequencia SQL per executar")
args = parser.parse_args()
# -------------------------------------------------------
#command = " psql -qtA -F ',' -h [ip_server_psql] -U postgres training -c \"select * from cliente\""
command = " psql -qtA -F',' -h 172.17.0.2 -U postgres training"
pipeData = Popen(command,shell=True, bufsize=0, universal_newlines=True, stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write("select * from oficinas;\n\q\n")

for line in pipeData.stdout:
  print(line, end="")
exit(0)
