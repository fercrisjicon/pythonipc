# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n nlin] [-f file]
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
# Exemple de programació Objectes POO
# -------------------------------------
class UnixUser():
    """Classe UnixUser: prototipus de 
    /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""

    def __init__(self, userLine):
        "Constructor objectes UnixUser"
        userField=userLine.split(":")
        
        self.login=userField[0]
        self.passwd=userField[1]
        self.uid=int(userField[2])
        self.gid=int(userField[3])
        self.gecos=userField[4]
        self.dirhome=userField[5]
        self.dirbash=userField[6]

    def show(self):
        "Mostrar les dades de l'usuari"
        print(f'login:{self.login} uid:{self.uid} gid:{self.gid} gecos:{self.gecos} home:{self.dirhome} bash:{self.dirbash}')

    def __str__(self):
        "Funció per retornar un string del objecte"
        return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.dirhome, self.dirbash)

print("Programa")

user1=UnixUser("root:x:0:0:root:/root:/bin/bash")
user1.show()
print(user1)
exit(0)
