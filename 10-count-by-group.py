# /usr/bin/python
#-*- coding: utf-8-*-
#
# count-by-group.py [-s gid|gname|nusers] -u users -g groups
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys, argparse

parser = argparse.ArgumentParser(description=\
        """Count-by-users.py llistat de grups per gid, gname o nusers""",
        epilog="thats all folks")
parser.add_argument("-s","--sort",type=str,\
        help="sort criteria: gid | gname | nusers", metavar="criteria",\
        choices=["gid","gname","nusers"],dest="criteria")
parser.add_argument("userFile",type=str,\
        help="user file (/etc/passwd style)", metavar="userFile")
parser.add_argument("groupFile",type=str,\
        help="user file (/etc/passwd style)", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
    """Classe UnixUser: prototipus de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
  
    def __init__(self,userLine):
        "Constructor objectes UnixUser"
        userField=userLine.split(":")
        self.login=userField[0]
        self.passwd=userField[1]
        self.uid=int(userField[2])
        self.gid=int(userField[3])
        self.gname=""
        if self.gid in groupDict:
            self.gname=groupDict[self.gid].gname
        self.gecos=userField[4]
        self.home=userField[5]
        self.shell=userField[6][:-1]
    
    def show(self):
        "Mostra les dades de l'usuari"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")

    def __str__(self):
        "functió to_string d'un objcete UnixUser"
        return "%s %s %d %d %s %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gname, self.gecos, self.home, self.shell)

# -------------------------------------------------------
class UnixGroup:
    """Classe UnixGroup: prototipus de /etc/passwd
    gidname:passwd:gid:list_user"""

    def __init__(self,groupLine):
        "Constructor objectes UnixGroup"
        groupField=groupLine.split(":")
        self.gname=groupField[0]
        self.passwd=groupField[1]
        self.gid=int(groupField[2])
        self.userListStr = groupField[3]
        self.userList=[]
        if self.userListStr[:-1]:
            self.userList = self.userListStr[:-1].split(",")

    def show(self):
        "Mostra les dades de l'usuari"
        print(f"gname: {self.gname} gid: {self.gid} llista d'usuaris: {self.list_usuaris}")

    def __str__(self):
        "functió to_string"
        return "%s %d %s" % (self.gname, self.gid, self.userList)

 
# NO FA FALTA -------------------------------------------------------
def sort_login(user):
    """Comparador d'usuaris per login"""
    return user.login

def sort_gid(user):
    """Comparador d'usuaris per gid"""
    return(user.gid, user.login) # primero ordena por gid y luego por login

def sort_gname(user):
  '''Comparador d'usuaris segons el gname'''
  return (user.gname, user.login) # primero ordena por gname y luego por login
# -------------------------------------------------------
groupFile=open(args.groupFile,"r")

groupDict={}
for line in groupFile:
  group=UnixGroup(line)
  groupDict[group.gid]=group
groupFile.close()
# -------------------------------------------------------
fileIn=open(args.userFile,"r")

userList=[]
for line in fileIn:
    user=UnixUser(line)
    userList.append(user)
    if user.gid in groupDict:
        if user.login not in groupDict[user.gid].userList:
            groupDict[user.gid].userList.append(user.login)
fileIn.close()
# ------------------------------------------------------
index=[]
if args.criteria=="gname":
    index = [ (groupDict[k].gname,k) for k in groupDict ]
elif args.criteria=="nusers":
    index = [ (len(groupDict[k].userList),k) for k in groupDict ]
else:
    index = [ k for k in groupDict ]
index.sort()
# ------------------------------------------------------
if args.criteria=="gname" or args.criteria=="nusers":
    for g,k in index:
        print(groupDict[k])
else:
    for k in index:
        print(groupDict[k])
exit(0)
