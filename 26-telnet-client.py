# /usr/bin/python3
#-*- coding: utf-8-*-
#
# telnet-client.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2021-2022
# Gener 2022
# -------------------------------------
import sys,socket,os,signal,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""Telnet Client""")
parser.add_argument("server",type=str)
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
FI = bytes(chr(4), 'utf-8')

s.connect((HOST, PORT))
while True:
    command = input("%s> " % HOST)
    if command == 'exit':
        break
    elif command == '':
        s.close()
        break
    else:
        s.send(bytes(command, 'utf-8'))

    while True:
        data = s.recv(1024)
        if data[-1:] == FI:
            print(str(data[:-1]))
            break
        print(data.decode("utf-8"))

s.close()
sys.exit(0)
